<?php
return [
    'default' => 'cms',
    'migrations' => 'migrations',
    'connections' => [
        'cms' => [
            'driver' => 'mysql',
            'host' => env('DB_HOST'),
            'database' => env('DB_DATABASE'),
            'username' => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
        ],
        'production' => [
            'driver' => 'mysql',
            'host' => env('DB2_HOST'),
            'database' => env('DB2_DATABASE'),
            'username' => env('DB2_USERNAME'),
            'password' => env('DB2_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
        ],
        'staging' => [
            'driver' => 'mysql',
            'host' => env('DB3_HOST'),
            'database' => env('DB3_DATABASE'),
            'username' => env('DB3_USERNAME'),
            'password' => env('DB3_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
        ],
        'info' => [
            'driver' => 'mysql',
            'host' => env('DB_INFO_HOST'),
            'database' => env('DB_INFO_DATABASE'),
            'username' => env('DB_INFO_USERNAME'),
            'password' => env('DB_INFO_PASSWORD'),
            'charset'   => 'utf8',
            'collation' => 'utf8_general_ci',
        ],
    ]
];
