# ProductCSVChecker

Three inputs are needed for the endpoint to work

1. upcColumn: The name of the column the UPCs are found in
2. brandColumn: The name of the column the brand names are found in
3. file: the input CSV file. Please ensure that the headers are on the 1st row
