<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\QueryException;

class TestController extends Controller
{

    public function productCSVChecker(Request $request)
    {
        $allowedEncoding = array(
            "UTF-8",
            "ASCII",
            "ISO-8859-1"
        );

        if (!$request->has('file')) {
            return response()->json("File missing", 400);
        }
        $csvContent = file_get_contents($request->file('file'));
        $csvEncoding = mb_detect_encoding($csvContent);

        //UPC Column Name
        if (!$request->has('upcColumn')) {
            return response()->json("upcColumn missing", 400);
        }
        $upcColumn = $request->input('upcColumn');
        //BrandName column Name
        if (!$request->has('brandColumn')) {
            return response()->json("brandColumn missing", 400);
        }
        $brandColumn = $request->input('brandColumn');
        //CountryCode
        if (!$request->has('countryCode')) {
            $countryCode = 'GLOBAL';
        } else {
            $countryCode = $request->input('countryCode');
        }

        if (!in_array($csvEncoding,$allowedEncoding)) {
            return response()->json('Upload CSV does not support Encoding: (' . (($csvEncoding == "") ? 'unknown' : $csvEncoding) . '). Please export your CSV using UTF-8 encoding', 400);
        }

        if ($csvEncoding != "UTF-8") {
            $csvContent = utf8_encode($csvContent);
        } else {
            // check to make sure UTF-8 since there might be some non UTF-8 characters
            // https://stackoverflow.com/questions/46305169/php-json-encode-malformed-utf-8-characters-possibly-incorrectly-encoded
            $csvContent = mb_convert_encoding($csvContent, 'UTF-8', 'UTF-8');
        }

        // write csvContent to a temporary file
        $tempCsvFile = tmpfile();
        fwrite($tempCsvFile, $csvContent);
        fseek($tempCsvFile, 0);

        // track CSV rows
        $csvData = array();
        // read the CSV temp file line by line
        while (($csvLineArray = fgetcsv($tempCsvFile)) !== false) {
            $csvData[] = $csvLineArray;
        }

        // load CSV data into formatted array
        $formattedArray = array();

        //Create an array that matches index to header
        // We only care about two columns?

        $headerRow = $csvData[0];
        $columnNumber = sizeof($headerRow);

        // Now we loop over everything and query to see if it's found
        for($entryi = 1;$entryi < sizeof($csvData);$entryi++) {
            
            $rowIndex = $entryi + 1;
            $entryData = array();
            
            foreach($headerRow as $index => $headerName){
                $value = $csvData[$entryi][$index];
                $entryData[$headerName] = $value;
            }

            $brandColumnData = $entryData[$brandColumn];
            // Find a brand in our db that may match this...
            // Get rid of parenthesis if they exist
            $brandColumnData = trim(preg_replace("/\([^)]+\)/","",$brandColumnData));

            $results = DB::connection('production')->table('brand')
                ->where('brand_name', $brandColumnData)
                ->first();

            if (!$results) {
                return response()->json('unable to find brand', 400);
            }
            $brandId = $results->brand_id;

            $upc = $entryData[$upcColumn];

            //Look in item for the upc
            $upcExists = DB::connection('production')->table('item')
                ->where('brand_id', $brandId)
                ->where('country_code', $countryCode)
                ->where('upc', $upc)
                ->first();
            
            if ($upcExists) {
                $csvData[$entryi][] = 'Yes';
            } else {
                $csvData[$entryi][] = 'No';
            }
        }
        
        // Then export the CSV again.
        $csvData[0][] = 'Found?';

        $csvFileName = "FILE-" . date("Ymd-His") . ".csv";
        $headers = array(
            "Content-type" => "text/csv; charset=utf-8",
            "Content-Disposition" => "attachment; filename=".$csvFileName,
            "Pragma" => "no-cache",
            "Expires" => "0"
        );

        $filePath = '../storage/app/'.$csvFileName;
        $tempCsvFile = fopen($filePath, 'w');
        fputcsv($tempCsvFile, $csvData[0]);

        for($i = 1;$i < sizeof($csvData);$i++) {
            $lineArray = array();

          fputcsv($tempCsvFile,$csvData[$i]);
        }

        fseek($tempCsvFile, 0);
        
        $response = response()->download($filePath, $csvFileName, $headers);
        // register_shutdown_function('unlink', storage_path('app/' . $csvFileName));
        return $response;
    }
}
